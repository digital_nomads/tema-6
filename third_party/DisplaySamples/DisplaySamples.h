#include "global.h"

namespace Spring
{
   class __declspec(dllexport) DisplaySamples
   {
   public:
      double static getNumberOfDisplaySamples(const double SampleRate, const double DisplayRate);

   private:
      DisplaySamples();
   };
}



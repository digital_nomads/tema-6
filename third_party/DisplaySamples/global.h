#pragma once
#include <QtCore/qglobal.h>

#ifdef DisplaySamples_EXPORTS
# define DisplaySamples_EXPORT_IMPORT_API Q_DECL_EXPORT
#else
# define DisplaySamples_EXPORT_IMPORT_API Q_DECL_IMPORT
#endif

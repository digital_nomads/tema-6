
#include "spring\Application\TrivialScene.h"
#include <qmessagebox.h>
#include "ui_trivial_scene.h"
#include "DisplaySamples.h"

namespace Spring
{
   TrivialScene::TrivialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
   {}


   void TrivialScene::createScene()
   {
      //create the UI
      const auto ui = std::make_shared<Ui_trivialScene>();
      ui->setupUi(m_uMainWindow.get());
      
      //setting a temporary new title
      m_uMainWindow->setWindowTitle(QString("TrivialScene"));

      //setting centralWidget
      centralWidget = ui->centralwidget;


      //connect btn's release signal to defined slot
      QObject::connect(ui->pushButtonDsiplay, SIGNAL(released()), this, SLOT(mf_DisplayButton()));

   }

   void TrivialScene::release()
   {
   
   }

   void TrivialScene::save()
   {
   }

   void TrivialScene::mf_DisplayButton()
   {
      QMessageBox Msgbox;
      Msgbox.setText(QString::number(DisplaySamples::getNumberOfDisplaySamples(boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]), boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]))));
      Msgbox.exec();
   }


   TrivialScene::~TrivialScene()
   {}



}
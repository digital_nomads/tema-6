#include <spring/Application/InitialScene.h>

#include "ui_initial_scene.h"

#include <fstream>
#include <boost/lexical_cast.hpp>
namespace Spring
{

	InitialScene::InitialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		//if (std::ifstream("SavedData.txt"))
			//load();
		//create the UI
		auto ui = std::make_unique<Ui_InitialScene>();
		ui->setupUi(m_uMainWindow.get());

		//connect btn's release signal to defined slot
		QObject::connect(ui->pushButtonOk, SIGNAL(released()), this, SLOT( mf_OkButton()));

		//set centralWidget
		centralWidget = ui->centralwidget;      
		unsigned int sampleRate = boost::any_cast<unsigned int>(m_TransientDataCollection.find("SampleRate")->second);


		auto sampleRateSpinBox = centralWidget->findChild<QSpinBox*>("sampleRateSpinBox");
		sampleRateSpinBox->setValue(sampleRate);

		if (m_TransientDataCollection.size() > 1)
		{
			double refreshRate = boost::any_cast<double>(m_TransientDataCollection.find("RefreshRate")->second);

			auto refreshRateSpinBox = centralWidget->findChild<QDoubleSpinBox*>("doubleSpinBoxRefreshRate");
			refreshRateSpinBox->setValue(refreshRate);


			double displayTime = boost::any_cast<double>(m_TransientDataCollection.find("DisplayTime")->second);

			auto displayTimeSpinBox = centralWidget->findChild<QDoubleSpinBox*>("doubleSpinBoxDisplayTime");
			displayTimeSpinBox->setValue(displayTime);


			std::string appName = boost::any_cast<std::string>(m_TransientDataCollection.find("ApplicationName")->second);
			auto applicationNameLineEdit = centralWidget->findChild<QLineEdit*>("lineEditName");
			applicationNameLineEdit->setText(QString::fromStdString(appName));
		}
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{

	}

	void InitialScene::mf_OkButton()
	{
		auto applicationNameLineEdit = centralWidget->findChild<QLineEdit*>("lineEditName");
		const std::string appName = applicationNameLineEdit->text().toStdString();

		if (!appName.empty())
		{
         m_TransientDataCollection["ApplicationName"] = appName;
		}


		//setting the new sample rate value in transient data
		auto sampleRateSpinBox = centralWidget->findChild<QSpinBox*>("sampleRateSpinBox");
		unsigned int sampleValue = sampleRateSpinBox->value();

		m_TransientDataCollection["SampleRate"] = sampleValue;

		auto displayTimeSpinBox = centralWidget->findChild<QDoubleSpinBox*>("doubleSpinBoxDisplayTime");
		double timeValue  = displayTimeSpinBox->value();

		m_TransientDataCollection["DisplayTime"] = timeValue;

		auto refreshRateSpinBox = centralWidget->findChild<QDoubleSpinBox*>("doubleSpinBoxRefreshRate");
		double refreshValue = refreshRateSpinBox->value();

		m_TransientDataCollection["RefreshRate"] = refreshValue;
		///TODO: set the other elements

		save();

		const std::string c_szNextSceneName = "Base scene";
		emit SceneChange(c_szNextSceneName);
	}

	void InitialScene::save()
	{
		std::ofstream _outputStream("SavedData.txt");
		for (std::map<const std::string, boost::any>::iterator it = m_TransientDataCollection.begin(); it != m_TransientDataCollection.end(); ++it)
		{
			if (it->first.compare("SampleRate") == 0)
				_outputStream << it->first << ' ' << boost::any_cast<unsigned int>(it->second) << std::endl;
			else
			{
				if (it->first.compare("ApplicationName") == 0)
					_outputStream << it->first << ' ' << boost::any_cast<std::string>(it->second) << std::endl;
				else
					_outputStream << it->first << ' ' << boost::any_cast<double>(it->second) << std::endl;
			}
		}
		_outputStream.close();
	}
	
}

#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"


namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

      //connect btn's release signal to defined slot
      QObject::connect(ui->pushButtonBack, SIGNAL(released()), this, SLOT(mf_BackButton()));
      QObject::connect(ui->pushButtonStart, SIGNAL(released()), this, SLOT(mf_StartButton()));
	  
	}

   void BaseScene::mf_BackButton()
   {

      const std::string c_szNextSceneName = "Initial scene";
      emit SceneChange(c_szNextSceneName);
   }

   void BaseScene::mf_StartButton()
   {
      const std::string c_szNextSceneName = "Trivial scene";
      emit SceneChange(c_szNextSceneName);
   }

   void BaseScene::save()
   {
   }


	void BaseScene::release()
	{
		delete centralWidget;
	}

	BaseScene::~BaseScene()
	{

	}
}

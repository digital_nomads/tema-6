
#include <spring\Application\ApplicationModel.h>
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <spring\Application\TrivialScene.h>

#include <fstream>
#include <boost/lexical_cast.hpp>

const std::string initialSceneName = std::string("Initial scene");
const std::string baseSceneName = std::string("Base scene");
const std::string trivialSceneName = std::string("Trivial scene");

namespace Spring
{
	ApplicationModel::ApplicationModel():IApplicationModel()
	{
		if (std::ifstream("SavedData.txt"))
			load();
	}

   ApplicationModel::~ApplicationModel()
   {
   }

	void ApplicationModel::defineScene()
	{
		m_initialScene = std::make_shared<InitialScene>(initialSceneName);
		m_secondScene = std::make_shared<BaseScene>(baseSceneName);
      m_trivialScene = std::make_shared<TrivialScene>(trivialSceneName);

		m_Scenes.emplace(initialSceneName, m_initialScene.get());
		m_Scenes.emplace(baseSceneName, m_secondScene.get());
      m_Scenes.emplace(trivialSceneName, m_trivialScene.get());
	}
	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::load()
	{
		std::ifstream _fileStream("SavedData.txt");
		std::string _key, _value;
		while (_fileStream >> _key >> _value)
		{
			if (_key.compare("SampleRate") == 0)
				m_TransientData.emplace(_key, boost::lexical_cast<unsigned int>(_value));// [_key] = boost::lexical_cast<int>(_value);
			else
			{
				if (_key.compare("DisplayTime") == 0 || _key.compare("RefreshRate") == 0)
					m_TransientData.emplace(_key, boost::lexical_cast<double>(_value));	//m_TransientDataCollection[_key] = boost::lexical_cast<double>(_value);
				else
					m_TransientData.emplace(_key, _value);
			}
		}
		_fileStream.close();
	}
	
	void ApplicationModel::defineTransientData()
	{
		if (std::ifstream("SavedData.txt"))
		{
			load();
		}
		else
		{
			unsigned int _sampleRate = 25000;
			m_TransientData["SampleRate"] = _sampleRate;
		}
	}
}

#pragma once
#include <spring\Framework\IScene.h>
#include <qobject.h>
namespace Spring
{
   class TrivialScene :public IScene
   {
      Q_OBJECT

   public:
      TrivialScene(const std::string& ac_szSceneName);

      virtual void createScene();

      virtual void release();

	  virtual void save();

      ~TrivialScene();

      public slots:

      void mf_DisplayButton();

   private:

      QWidget *centralWidget;
   };
}



#pragma once
#include <spring\Framework\IApplicationModel.h>
#include <spring\Application\global.h>
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <spring\Application\TrivialScene.h>

namespace Spring
{
	class Application_EXPORT_IMPORT_API ApplicationModel : public IApplicationModel
	{
	public:

		ApplicationModel();
      ~ApplicationModel();

		virtual void defineScene();

		virtual void defineInitialScene();

		virtual void load();

		virtual void defineTransientData();

	private:
		std::shared_ptr<InitialScene> m_initialScene;
		std::shared_ptr<BaseScene>    m_secondScene;
      std::shared_ptr<TrivialScene> m_trivialScene;
	};
}
